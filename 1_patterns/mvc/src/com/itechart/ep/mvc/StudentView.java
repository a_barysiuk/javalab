package com.itechart.ep.mvc;

public class StudentView {

	public void show(Student student) {
		System.out.println("==============" + student.getName() + "================");
		System.out.println("Roll number is " + student.getRollNo());
		System.out.println();
	}

}
