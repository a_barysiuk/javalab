package com.itechart.ep.mvc;

public class MVCDemo {

	public static void main(String[] args) {

		Student model = new Student("Karl", 4);
		StudentView view = new StudentView();

		StudentController controller = new StudentController(model, view);

		controller.updateView();

		controller.setStudentName("John");
		controller.updateView();

		controller.setStudentRollNo(666);
		controller.updateView();
	}

}
