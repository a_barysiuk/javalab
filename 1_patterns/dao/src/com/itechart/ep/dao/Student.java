package com.itechart.ep.dao;

public class Student {

	private String name;
	private int rollNo;

	public Student(String name, int rollNo){
		this.name = name;
		this.rollNo = rollNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getRollNo() {
		return rollNo;
	}

	public void setRollNo(int rollNo) {
		this.rollNo = rollNo;
	}

	@Override
	public String toString() {
		final StringBuffer sb = new StringBuffer("Student {");
		sb.append("name='").append(name).append(", rollNo=").append(rollNo).append('}');
		return sb.toString();
	}
}
