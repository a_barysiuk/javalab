package com.itechart.ep.ifilter;

public class DebugFilter implements Filter {

	@Override
	public void apply(String request) {
		System.out.println("Request log: " + request);
	}
}
