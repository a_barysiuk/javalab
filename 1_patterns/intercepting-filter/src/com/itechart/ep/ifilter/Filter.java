package com.itechart.ep.ifilter;

public interface Filter {
	public void apply(String request);
}
