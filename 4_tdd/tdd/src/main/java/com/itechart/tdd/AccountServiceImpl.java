package com.itechart.tdd;


public class AccountServiceImpl implements AccountService {

	public void deposit(Account account, int amount) {
		
		if(amount < 0)
			throw new IllegalArgumentException("Amount should be gretaer than 0");
		
		account.setBalance(account.getBalance() + amount);
	}

	public void withdraw(Account account, int amount) throws AccountBalanceException {
		
		if(amount < 0)
			throw new IllegalArgumentException("Amount should be gretaer than 0");
		
		if(amount > account.getBalance())
			throw new AccountBalanceException(account, "Not enought money!");
		account.setBalance(account.getBalance() - amount);
	}

}
