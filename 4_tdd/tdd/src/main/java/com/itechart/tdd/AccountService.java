package com.itechart.tdd;

public interface AccountService {

	public void deposit(Account account, int amount);

	public void withdraw(Account account, int amount) throws AccountBalanceException;

}
