package mock;

import com.itechart.tdd.Account;
import com.itechart.tdd.AccountBalanceException;
import com.itechart.tdd.AccountService;

public class AccountServiceMockImpl implements AccountService {

	private AccountDao accountDao;
	
	public AccountServiceMockImpl(AccountDao accountDao) {
		this.accountDao = accountDao;
	}

	public void deposit(Account account, int amount) {
		
		if(amount < 0)
			throw new IllegalArgumentException("Amount should be gretaer than 0");
		
		account.setBalance(account.getBalance() + amount);
		
		accountDao.save(account);
	}

	public void withdraw(Account account, int amount) throws AccountBalanceException {
		
		if(amount < 0)
			throw new IllegalArgumentException("Amount should be gretaer than 0");
		
		if(amount > account.getBalance())
			throw new AccountBalanceException(account, "Not enought money!");
		
		account.setBalance(account.getBalance() - amount);
		
		accountDao.save(account);
	}

}
