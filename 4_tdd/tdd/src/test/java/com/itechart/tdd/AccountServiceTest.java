package com.itechart.tdd;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class AccountServiceTest {

	private AccountService accountService = new AccountServiceImpl();
	private Account account;
	
	public AccountServiceTest() {
		System.out.println("Constructor");
	}
	
	@BeforeClass
	public static void setUp() {
		System.out.println("Before class");
	}
	
	@AfterClass
	public static void tearDown() {
		System.out.println("After class");
	}
	
	@Before
	public void before() {
		System.out.println("Before");
		account = new Account("test account", 1000);
	}
	
	@After
	public void after() {
		System.out.println("After");
		System.out.println(account);
	}

	@Test
	public void shouldAddAmountToBalance() {
		
		System.out.println("Test 1");

		accountService.deposit(account, 2000);

		Assert.assertEquals(3000, account.getBalance());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void shouldFailDepositIfAmountLessThanZero() {
		
		System.out.println("Test 2");

		accountService.deposit(account, -2000);
	}
	
	@Test
	public void shouldSubtractAmountFromBalance() throws AccountBalanceException {
		
		System.out.println("Test 3");

		accountService.withdraw(account, 500);
		
		Assert.assertEquals(500, account.getBalance());
	
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void shouldFailWithrawIfAmountLessThanZero() throws AccountBalanceException {
		System.out.println("Test 4");

		accountService.withdraw(account, -2000);
	}
	
	@Test(expected = AccountBalanceException.class)
	public void shouldFailWithdrawIfBalanceIsLessThanZero() throws AccountBalanceException {
		System.out.println("Test 5");

		accountService.withdraw(account, 2000);
	}

}
