package com.itechart.tdd.mock;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import mock.AccountDao;
import mock.AccountServiceMockImpl;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.itechart.tdd.Account;
import com.itechart.tdd.AccountBalanceException;
import com.itechart.tdd.AccountService;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceMockTest {

	private AccountService accountService;

	@Mock
	private AccountDao accountDao;

	@Before
	public void setUp() {
		accountService = new AccountServiceMockImpl(accountDao);
	}

	@After
	public void tearDown() {
		accountDao = null;
		accountService = null;
	}

	@Test
	public void shouldSubtractAmountFromBalanceAndSave() throws AccountBalanceException {
			
		when(accountDao.save(any(Account.class))).then(new Answer<Account>() {

			public Account answer(InvocationOnMock invocation) throws Throwable {
				Object[] arguments = invocation.getArguments();
				
				Account a = (Account) arguments[0];
				a.setId(1L);
				
				return a;
			}
		});

		Account account = new Account("test account", 1000);
		
		System.out.println(account);

		accountService.withdraw(account, 500);

		assertEquals(500, account.getBalance());

		System.out.println(account);

	}

}
