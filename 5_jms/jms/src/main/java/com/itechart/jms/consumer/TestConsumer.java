package com.itechart.jms.consumer;

import javax.jms.Message;
import javax.jms.MessageListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestConsumer implements MessageListener {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	public void onMessage(Message msg) {
		logger.debug("Message: {}", msg);
	}

}
