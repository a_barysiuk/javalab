package com.itechart.jms;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.itechart.jms.producer.TestProducer;

public class ProducerApp {

	public static void main(String[] args) {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
		TestProducer testProducer = context.getBean(TestProducer.class);
		testProducer.send();
	}

}
