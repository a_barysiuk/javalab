package com.itechart.jms.consumer;

import javax.jms.Message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.core.JmsTemplate;

public class QueueTestConsumer {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private JmsTemplate template;
	private String queue;
	
	public void receive() throws InterruptedException {
		Message message = template.receive(queue);
		logger.debug("Queue message: {}", message);
	}

	public void setTemplate(JmsTemplate template) {
		this.template = template;
	}

	public void setQueue(String queue) {
		this.queue = queue;
	}
	
	

}
