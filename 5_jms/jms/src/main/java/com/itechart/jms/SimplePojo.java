package com.itechart.jms;

import java.io.Serializable;

public class SimplePojo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String name;
	private Long number;

	public SimplePojo(String name, Long number) {
		this.name = name;
		this.number = number;
	}

	@Override
	public String toString() {
		return "SimplePojo [name=" + name + ", number=" + number + "]";
	}

}
