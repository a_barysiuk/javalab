package com.itechart.jms.producer;

import java.util.ArrayList;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.Topic;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import com.itechart.jms.SimplePojo;

public class TestProducer {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private JmsTemplate template;
	private Queue queue;
	private Topic topic;

	public void send() {
		sendToQueue();
		sendToTopic();
	}

	private void sendToTopic() {
		logger.debug("Send message to the topic");
		template.send(topic, new MessageCreator() {

			public Message createMessage(Session session) throws JMSException {
				MapMessage mapMessage = session.createMapMessage();
				mapMessage.setStringProperty("name", "topic message");
				mapMessage.setString("string", "String value");
				mapMessage.setLong("long", 20L);
				mapMessage.setObject("list", new ArrayList<String>() {{add("1"); add("2"); add("3");}});
				return mapMessage;
			}
		});

	}

	private void sendToQueue() {
		logger.debug("Send message to the queue");
		template.send(queue, new MessageCreator() {

			public Message createMessage(Session session) throws JMSException {
				ObjectMessage objectMessage = session
						.createObjectMessage(new SimplePojo(
								"test queue simple pojo", 999L));
				return objectMessage;
			}
		});
	}

	public void setTemplate(JmsTemplate template) {
		this.template = template;
	}

	public void setQueue(Queue queue) {
		this.queue = queue;
	}

	public void setTopic(Topic topic) {
		this.topic = topic;
	}
	
	
}
