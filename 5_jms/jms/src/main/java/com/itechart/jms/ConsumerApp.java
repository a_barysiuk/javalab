package com.itechart.jms;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.itechart.jms.consumer.QueueTestConsumer;

public class ConsumerApp {
	
	public static void main(String[] args) throws InterruptedException {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
		QueueTestConsumer testConsumer = context.getBean(QueueTestConsumer.class);
		testConsumer.receive();
	}

}
