package com.itechart.maven.spring.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.itechart.maven.spring.dao.Dao;
import com.itechart.maven.spring.model.Contact;

@Controller
@RequestMapping("/contact")
public class ContactController {

	@Autowired
	@Qualifier("contactDao")
	private Dao<Contact> contactDao;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView get() {

		final Collection<Contact> list = contactDao.findAll();
		ModelAndView mav = new ModelAndView("contact");
		mav.addObject("items", list);
		return mav;
	}

	public Dao<Contact> getContactDao() {
		return contactDao;
	}

	public void setContactDao(Dao<Contact> contactDao) {
		this.contactDao = contactDao;
	}

}
