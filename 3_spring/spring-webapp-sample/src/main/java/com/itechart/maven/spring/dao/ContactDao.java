package com.itechart.maven.spring.dao;

import java.util.Collection;
import java.util.Map;

import com.itechart.maven.spring.model.Contact;

public class ContactDao implements Dao<Contact> {
	
	private final Map<Long, Contact> storage;
	
	public ContactDao(Map<Long, Contact> initialStorage) {
		this.storage = initialStorage;
	}

	public void saveOrUpdate(Contact contact) {
		this.storage.put(contact.getId(), contact);
	}

	public Contact findById(Long id) {
		return this.storage.get(id); 
	}

	public void deleteById(Long id) {
		this.storage.remove(id);
	}

	public Collection<Contact> findAll() {
		return storage.values();
	}

}
