package com.itechart.maven.spring.dao;

import java.util.Collection;

import com.itechart.maven.spring.model.Entity;

public interface Dao<T extends Entity> {
	
	public void saveOrUpdate(T entity);
	public T findById(Long id);
	public Collection<T> findAll();
	public void deleteById(Long id);	

}
