package com.itechart.maven.spring.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.itechart.maven.spring.dao.Dao;
import com.itechart.maven.spring.model.Address;

@Controller
@RequestMapping("/address")
public class AddressController {

	@Autowired
	@Qualifier("addressDao")
	private Dao<Address> addressDao;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView get() {

		final Collection<Address> list = addressDao.findAll();
		ModelAndView mav = new ModelAndView("address");
		mav.addObject("items", list);
		return mav;
	}

}
