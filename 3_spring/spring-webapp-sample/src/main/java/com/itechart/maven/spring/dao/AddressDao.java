package com.itechart.maven.spring.dao;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.itechart.maven.spring.model.Address;

@Repository
@Qualifier("addressDao")
public class AddressDao implements Dao<Address> {

	private Map<Long, Address> storage;

	public AddressDao() {
		storage = new HashMap<Long, Address>(3);
		addAddress(1L, "Belarus", "220000", "Minsk");
		addAddress(2L, "France", "111000", "Paris");
		addAddress(3L, "Germany", "432000", "Munich");
	}

	private void addAddress(Long id, String country, String zip, String city) {
		storage.put(id, new Address(id, country, zip, city));
	}

	public void saveOrUpdate(Address address) {
		storage.put(address.getId(), address);

	}

	public Address findById(Long id) {
		return storage.get(id);
	}

	public void deleteById(Long id) {
		storage.remove(id);
	}

	public Collection<Address> findAll() {
		return storage.values();
	}

}
