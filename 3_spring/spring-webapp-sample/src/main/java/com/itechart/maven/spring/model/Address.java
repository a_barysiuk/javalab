package com.itechart.maven.spring.model;

public class Address extends Entity {

	private String country;
	private String zip;
	private String city;

	public Address(Long id, String country, String zip, String city) {
		super(id);
		this.country = country;
		this.zip = zip;
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

}
