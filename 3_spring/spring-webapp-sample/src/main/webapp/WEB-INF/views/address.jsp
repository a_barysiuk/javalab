<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<title>Address</title>
</head>
<body>
${msg}
	<table>
		<thead>
			<tr>
				<th>Country</th>
				<th>City</th>
				<th>Zip</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="address" items="${items}">
				<tr>
					<td>${address.country}</td>
					<td>${address.city}</td>
					<td>${address.zip}</td>
				</tr>
			</c:forEach>
		</tbody>

	</table>
</body>
</html>