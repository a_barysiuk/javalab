<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<title>Contact</title>
</head>
<body>
	<table>
		<thead>
			<tr>
				<th>Name</th>
				<th>Phone</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="contact" items="${items}">
				<tr>
					<td>${contact.name}</td>
					<td>${contact.phone}</td>
				</tr>
			</c:forEach>
		</tbody>

	</table>
</body>
</html>