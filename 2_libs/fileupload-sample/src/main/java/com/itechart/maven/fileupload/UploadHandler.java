package com.itechart.maven.fileupload;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

public class UploadHandler extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		System.out.println("Do post");

		
		boolean isMultipart = ServletFileUpload.isMultipartContent(request);

		response.setContentType("text/plain");
		PrintWriter out = response.getWriter();

		if(isMultipart) {
			
			// Create a factory for disk-based file items
			DiskFileItemFactory factory = new DiskFileItemFactory();

			// Configure a repository (to ensure a secure temp location is used)
			ServletContext servletContext = this.getServletConfig().getServletContext();
			String repositoryPath = servletContext.getInitParameter("repository");
			String uploadPath = servletContext.getInitParameter("upload.dir");
			factory.setRepository(new File(repositoryPath));
			
			System.out.println(uploadPath);

			// Create a new file upload handler
			ServletFileUpload upload = new ServletFileUpload(factory);

			// Parse the request
			try {
				List<FileItem> items = upload.parseRequest(request);
				for(FileItem item : items) {
					
					if(item.isFormField()) {
						out.println("Form field: " + item.getFieldName() + ", value: " + item.getString());
					} else {
						item.write(new File(uploadPath + "/" + item.getName()));
						out.println("File: " + item.getName() + ", size: " + item.getSize());
					}
				}
			} catch (FileUploadException e) {
				e.printStackTrace();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		} else {
			out.println("No files has been uploaded");
		}

		
	}

}
