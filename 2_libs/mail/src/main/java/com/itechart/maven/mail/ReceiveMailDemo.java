package com.itechart.maven.mail;

import java.io.IOException;
import java.util.Properties;

import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;

public class ReceiveMailDemo {


	public static void main(String[] args) {

		Properties props = new Properties();
		props.setProperty("mail.store.protocol", "imaps");
		
		
		try {
			Session session = Session.getDefaultInstance(props);
			Store store = session.getStore("imaps");
			store.connect("imap.gmail.com", "your_account@gmail.com", "password");

			Folder folder = store.getFolder("inbox");
			folder.open(Folder.READ_ONLY);

			Message[] messages = folder.getMessages(1,5);
			for (int i = 0; i < messages.length; i++) {
				Message message = messages[i];
				System.out.println("---------------------------------");
				System.out.println("Email Number " + (i + 1));
				System.out.println("Subject: " + message.getSubject());
				System.out.println("From: " + message.getFrom()[0]);
				System.out.println("Text: " + message.getContent().toString());
			}

			folder.close(false);
			store.close();

		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
