package com.itechart.maven.logging.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractService {
	
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	public AbstractService() {
		logger.info("Initialize");
	}
	
	protected abstract void service();
	
	
	public void run() {
		logger.debug("start...");
		
		service();
		
		logger.debug("end...");
	}
	
	

}
