package com.itechart.maven.logging.service.good;

import com.itechart.maven.logging.service.AbstractService;

public class GoodService extends AbstractService {

	@Override
	protected void service() {
		logger.debug("entering good service method");
		
		logger.info("do something good");
		
		int a = 1; 
		int b = 5;
		
		try {
			
			int x = a + b;
			
			logger.debug("Result is {}", x);
			
		} catch(ArithmeticException e) {
			logger.error("Wow!", e);
		} finally {
			logger.debug("leaving good service method");
		}
		
	}

}
