package com.itechart.maven.logging.service.bad;

import com.itechart.maven.logging.service.AbstractService;

public class BadService extends AbstractService {

	@Override
	protected void service() {
		logger.debug("entering bad service method");
		
		logger.info("do something bad");
		
		int a = 1; 
		int b = 0;
		
		try {
			float x = a/b;
		} catch(ArithmeticException e) {
			logger.error("Wow!", e);
		} finally {
			logger.debug("leaving bad service method");
		}
	}

}
