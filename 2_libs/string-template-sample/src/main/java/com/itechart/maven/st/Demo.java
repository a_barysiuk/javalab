package com.itechart.maven.st;

import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.STGroupDir;
import org.stringtemplate.v4.STGroupFile;

public class Demo {
	
	public static void main(String[] args) {
		inlineTemplate();
		loadFromFile();
//		loadFromDir();
	}
	
	private static void inlineTemplate() {
		ST hello = new ST("Hello, <name>");
		hello.add("name", "World");
		System.out.println(hello.render());
	}
	
	private static void loadFromFile() {
		STGroup group2 = new STGroupFile("group2.stg");
		
		ST t1 = group2.getInstanceOf("t1");
		t1.add("arg1", "This is template 1");
		t1.add("arg2", 222);
		System.out.println(t1.render());
	
		ST t2 = group2.getInstanceOf("t2");
		t2.add("arg1", "This is template 2");
		System.out.println(t2.render());
		
		ST t3 = group2.getInstanceOf("t3");
		t3.add("arg1", "This is template 3");
		System.out.println(t3.render());
		
	}
	
	private static void loadFromDir() {
		STGroup group1 = new STGroupDir("group1");
		ST address = group1.getInstanceOf("address");
		address.add("city", "Minsk");
		address.add("street", "Masherov ave.");
		address.add("number", "115-23");
		System.out.println(address.render());
		
	}

}
